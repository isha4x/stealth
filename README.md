# Stealth for Reddit

<p align="center"><img src="app/src/main/res/mipmap-xxxhdpi/ic_launcher_round.png" width="150"></p>
<p align="center"><img src="stealth.png" width="150" hspace="10" vspace="10"></p> 

Stealth is an account-free, privacy-oriented, and feature-rich Reddit client. 

It provides features from Reddit, without the need for an account. In fact, the app doesn't even let you log in with a Reddit account.


## Screenshots

<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/01_home.png" width="200" hspace="10"/>

<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/02_subscriptions.png" width="200" hspace="10"/>

<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/03_post.png" width="200" hspace="10"/>

<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/04_sub_1.png" width="200" hspace="10"/>

<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/05_sub_2.png" width="200" hspace="10"/>

<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/06_search.png" width="200" hspace="10"/>

<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/07_preferences.png" width="200" hspace="10"/>

## Features

- Browse Reddit (view posts, comments, subreddits, and users)
- Search posts, subreddits and users
- Subscribe to subreddits
- Sort
- History
- Show/hide NSFW content
- Image flairs
- Awards
- Light/Dark theme

 ### More to come

 - Save posts and comments
 - Save photos and videos
 - Remind-Me
 - and more...

## License

Copyright 2021 CosmosDev

Licensed under the GPLv3: http://www.gnu.org/licenses/gpl-3.0.html
